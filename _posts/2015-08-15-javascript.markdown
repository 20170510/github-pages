>* 编辑：刘彬
>* 时间：20xx年x月x日
>* 状态：未完成
><br>
从JavaScript访问某个Html元素可以使用 document.getElementById(id) 方法。<br>
document.getElementById("demo") 是使用 id 属性来查找 HTML 元素的 JavaScript 代码 。<br>
innerHTML = "内容。。。" 是用于修改元素的 HTML 内容(innerHTML)的 JavaScript 代码。<br>
document.write() 仅仅向文档输出写内容。<br>
使用console.log()方法在浏览器中显示 JavaScript 值。<br>
Javascript语法：<br>
数字(Number)字面量可以是整数或者小数，或者是科学计数(e)<br>
String str = “Hello JS”;//String str = ‘Hello JS’;（可以使用单引号和双引号）<br>
数组(Array)字面量[40, 100, 1, 5, 25, 10]<br>
对象（Object）字面量{firstName:"John", lastName:"Doe", age:50, eyeColor:"blue"}<br>
函数(Function)字面量function myFunction(a, b) { return a * b;}<br>
Javascript使用var来定义变量，使用等号来为变量赋值。<br>
Javascript标识符必须以字母、下划线(_)或美元符号($)开始。<br>
Javascript对大小写敏感！<br>
Javascript常见的是驼峰法的命名规范，使用的字符集编码是Unicode<br>
Javascript中，用分号结束语句是可选的。<br>
Javascript会忽略多余的空格。文本字符串中使用“\”对代码进行换行<br>
Javascript是脚本语言，浏览器会在读取代码时，逐行的执行脚本代码。而传统编程，会在执行前对代码进行编译。<br>
未使用值来声明的变量，其实际值是undefined<br>
如果从新声明javascript变量，该变量的值不会丢失。Var carname=”Volvo”; Var carname; 其值仍为Volvo<br>
Javascript数据类型<br>
字符串(String)、数字(Number)、布尔(Boolean)、数组(Array)、对象(Object)、空(Null)、未定义(Undefined)<br>
可以在字符串中使用引号，只要不匹配包围字符串的引号即可。<br>
创建数组 var cars = [“s”,”v”,”b”];<br>
Javascript对象由花括号分隔。在括号内部，对象的属性以名称和值对的形式(name:value)来定义。例如: var person={firstname:"John", lastname:"Doe", id:5566};<br>
对象属性有两种寻址方式：name=person.lastname; name=person[“lastname”];<br>
Undefined值表示变量不含有值。可以通过将变量的值设置为null来清空变量。<br>
当声明新变量时，可以使用关键词”new”来声明其类型。例：var carname=new String;<br>
Javascript变量均为对象，当声明一个变量时，就创建了一个新的对象。<br>
对象也是变量，对象可以包含多个值，如：var car = {type:"Fiat", model:500, color:"white"};<br>
如果把值赋给尚未声明的变量，该变量将被自动作为全局变量声明。<br>
全局变量或者函数，可以覆盖window对象的变量或者函数。局部变量，包括window对象可以覆盖全局变量和函数。<br>
比较运算符： 绝对等于(===)值和类型均相等<br>
绝对不等于(!==)值和类型均不相等<br>
Javascript中for/in语句循环遍历对象的属性<br>
JavaScript标签，语句前加冒号（：）<br>
在JavaScript中，数组是一种特殊的对象类型，typeof[1,2,3]返回Object<br>
用typeof检测null返回Object<br>
JavaScript数据类型：5种不同的数据类型（String、number、boolean、object、function）3种对象类型（Object、Date、Array）2种不包含任何值的数据类型（null、undefined）注：NaN的数据类型是number<br>
Constructor属性返回所有JavaScript变量的构造函数。<br>
全局方法String()可以将数字转换为字符串。Number方法toString()也是有同样的效果<br>
JavaScript正则表达式：<br>
在JavaScript中，正则表达式通常用于两个字符串方法：search()和replace()<br>
Search()方法用于检索字符串中指定的子字符串，或检索与正则表达式相匹配的子字符串，并返回子字符串的起始位置。<br>
Replace()方法用于子字符串中用一些字符替换另一些字符，或替换一个与正则表达式匹配的子串。<br>
正则表达式修饰符：i(执行对大小写不敏感的匹配);g(执行全局匹配，查找所有匹配而非在找到第一个匹配后停止);m(执行多行匹配)<br>
正则表达式模式：[abc]查找方括号之间的任何字符。[0-9]查找任何从0-9的数字。(x|y)查找任何以|分隔的选项。<br>
元字符：\d查找数字。\s查找空白字符。\b匹配单词边界。\uxxxx查找以十六进制数xxxx规定的Unicode字符。<br>
量词：n+匹配任何包含至少一个n的字符串;n*匹配任何包含零个或多个n的字符串;n?匹配任何包含零个或一个n的字符串。<br>
test()方法是一个正则表达式方法。test()方法用于检测一个字符串是否匹配某个模式，如果字符串中含有匹配的文本，则返回 true，否则返回 false。
JavaScript调试：console.log()方法在调试窗口打印JavaScript的值。设置断点。<br>
Debugger关键字用于停止执行JavaScript，并调用调试函数。<br>
JOSN语法格式：1、数据与键/值对。2、数据与逗号分隔。3、大括号保存对象。4、方括号保存数组。<br>
href=”#”与href=”javascript:void(0)”的区别：<br>
#包含了一个位置信息，默认的锚是#top也就是网页的上端；而JavaScript:void(0)<br>仅仅表示一个死链接。在页面很长的时候就会使用#来定位页面的具体位置，格式为：#+id<br>
JavaScript函数有属性和方法，有个内置的对象arguments对象<br>
this关键字指向函数执行时的当前对象（this是保留关键字，不能修改this的值）。<br>
当函数没有被自身的对象调用时，this的值就会变成全局对象，在web浏览器中全局对象是浏览器窗口(window对象)，该实例返回this的值是window对象。<br>
函数作为全局对象调用，会使this的值成为全局对象。使用window对象作为一个变量容易造成程序崩溃。<br>
函数作为对象方法调用，会使得this的值成为对象本身。<br>
构造函数的调用会创建一个新的对象，新对象会继承构造函数的属性和方法。<br>
构造函数中this关键字没有任何值。this的值在函数调用时实例化对象(new object)时创建。<br>
JavaScript函数有它的属性和方法，call()和apply()<br>是预定义的函数方法。两个方法可用于调用函数，两个方法的第一个参数必须是对象本身。两者的区别在于第二个参数：apply()<br>传入的是一个参数数组，也就是将多个参数组合成一个数组传入。而call则作为call的参数传入。<br>
变量声名如果不使用var关键字，那么它就是全局变量，即便是在函数内部定义。<br>
闭包是可以访问上一层函数作用域里变量的函数，即便上一层函数已经关闭。<br>
HTML DOM(Document Object Model)文档对象模型<br>
通过JavaScript操作HTML元素：<br>
1、通过id找到HTML元素(document.getElementById())<br>
2、通过标签名找到HTML元素(document.getElementsByTagName())<br>
3、通过类名找到HTML元素(document.getElementsByClassName())<br>
绝对不要在文档加载完成之后使用document.write()，这会覆盖该文档。<br>
改变HTML元素的样式：document.getElementById(id).style.property = new style<br>
事件冒泡或事件捕获<br>
事件传递的两种方式：冒泡和捕获<br>
例如：事件传递定义了元素事件触发顺序。如果将`<p>`元素插入到`<div>`元素中用户点击 `<p>`元素, 哪个元素的 "click" 事件先被触发呢？<br>
1、在冒泡中，内部元素的事件会先被触发，然后再触发外部元素，即：`<p>`元素的点击事件先触发，然后触发`<div>`元素的点击事件。<br>
2、在捕获中，外部元素的事件会先被触发，然后才会触发内部元素的事件，即：`<div>`元素的事件先触发，然后触发`<p>`元素的点击事件。addEventListener()方法可以指定”useCapture”参数来设置传递类型addEventListener(event, function, useCapture);默认值为false，即冒泡传递，当值为true时，事件使用捕获传递。<br>
JavaScript高级教程<br>
可以使用toString()方法输出16进制、8进制和2进制<br>
myNumber.toString(16/8/2);<br>
无穷大(Infinity)：运算结果超过了JavaScript所能表示的数字上限(溢出)，结果为一个特殊的无穷大(Infinity值)。。。。。。。。。。略。<br>
浏览器BOM<br>
BOM(Browser Object Model)浏览器对象模型<br>
Window对象：<br>
1、所有浏览器都支持window对象，它表示浏览器窗口。<br>
2、所有JavaScript全局对象、函数以及变量均自动生成为window对象的成员。<br>
3、全局变量是window对象的属性<br>
4、全局函数是window对象的方法。<br>
确定浏览器的尺寸：window.innerHeight(内部高度)和window.innerWidth(内部宽度)<br>
Window对象有(screen、Location、History、Navigator、弹框、计时事件)<br>
Navigator对象的信息具有误导性，不应该被用于检测浏览器的版本，原因：<br>
1、navigator数据可被浏览器使用者更改<br>
2、一些浏览器对测试站点会识别错误<br>
3、浏览器无法报告晚于浏览器发布的新操作系统。<br>
弹框：警告框alert、确认框confirm、提示框prompt<br>
计时：setInterval()和setTimeout()是HTML DOM Window对象的两个方法<br>
setInterval()-间隔指定的毫秒数不停的执行指定的代码。<br>
setTimeout()-暂停指定的毫秒数后执行指定的代码。<br>
clearInterval()方法用于停止setInterval()方法执行的函数代码。<br>
clearTimeout()方法用于停止执行setTimeout()函数的执行。<br>
Cookies：<br>
用于存储web页面的用户信息。<br>
使用document.cookie属性来创建、读取、修改及删除cookie。<br>
1、创建Cookie：<br>
document.cookie = “username=John Don; expires = 添加过期时间，默认为浏览器关闭删除cookie; path=/”;<br>
2、读取Cookie：<br>
Var x = document.cookie;将以字符串的方式返回所有的Cookies，类型格式为cookie1=value；cookie2 = value。<br>
3、修改cookie<br>
类似于创建cookie，旧的cookie将会被覆盖。<br>
4、删除cookie<br>
只需要设置expires参数为以前的时间即可。删除时不必指定cookie的值。<br>
<br>