> #### underscore
>* 编辑：刘彬
>* 时间：20xx年x月x日
>* 状态：未完成
>###常用函数解析
map ： _.map(list, interatee, [context])  别名：collect  //对list中每个值做规则处理，返回一个新的数组
例： 
```javascript
_.map([1, 2, 3], function(num){ return num * 3; });
=> [3, 6, 9]
_.map({one: 1, two: 2, three: 3}, function(num, key){ return num * 3; });
=> [3, 6, 9]
```
reduce ： _.reduce(list, iteratee, [memo], [context])  //返回对list的处理结果，得到一个值
例：
```javascript
var sum = _.reduce([1, 2, 3], function(memo, num){ return memo + num; }, 0);
=> 6
```
find ： _.find(list, predicate, [context])  //返回list中第一个符合规则的值，得到一个值。
例： 
```javascript
var even = _.find([1, 2, 3, 4, 5, 6], function(num){ return num % 2 == 0; });
=> 2
```
filter ： _.filter(list, predicate, [context]) //返回list中所有符合规则的值，得到一个数组。
例： 
```javascript
var evens = _.filter([1, 2, 3, 4, 5, 6], function(num){ return num % 2 == 0; });
=> [2, 4, 6]
```
every ： _.every(list, [predicate], [context])  //对list中每一个值做校验，全部为true则返回true
例： 
```javascript
_.every([true, 1, null, 'yes'], _.identity);
=> false
```
pluck ： _.pluck(list, propertyName)  //获取list中属性为propertyName的值，返回一个数组。
例： 
```javascript
var stooges = [{name: 'moe', age: 40}, {name: 'larry', age: 50}, {name: 'curly', age: 60}];
_.pluck(stooges, 'name');
=> ["moe", "larry", "curly"]
```
omit ： _.omit(object, *keys)   //object中去掉含有keys的属性
例： 
```javascript
_.omit({name: 'moe', age: 50, userid: 'moe1'}, 'userid');
=> {name: 'moe', age: 50}
_.omit({name: 'moe', age: 50, userid: 'moe1'}, function(value, key, object) {
  return _.isNumber(value);
});
=> {name: 'moe', userid: 'moe1'}
```
链式语法：
chain ： _.chain()
返回一个封装的对象。在封装的对象上调用方法会返回对象本身，知道value方法调用。
```javascript
_.chain()
    .map()
        .map()
            .value();
```
链式调用记得后面加value()，否则返回的是被chain包装过的数组，加了value后返回的是一个普通的数组。
```javascript
value ： _(obj).value();
_([1,2,3]).value();
=>[1,2,3]
```






