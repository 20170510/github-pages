>## [设计方案]
* 编辑：刘彬
* 时间：20xx年x月x日
* 状态：未完成
><br>
Spring 学习笔记<br>
Spring是一个开源框架，是为了解决企业应用程序开发复杂性而创建的。<br>
Spring的主要优势：分层架构，分层架构允许选择使用哪一个组件，同时为J2EE应用程序开发提供集成的框架。<br>
Spring框架分7个模块，每个模块的功能：<br>
核心容器：提供spring框架的基本功能。主要组件是BeanFactory，BeanFactory使用控制反转（IOC）模式将应用程序的配置和依赖性规范与实际的应用程序代码分开。Inversion of control（Dependence Injection）<br>
Spring上下文：spring上下文是一个配置文件，向spring框架提供上下文信息。<br>
Spring AOP：<br>
>
Spring注入：<br>
1、构造方法<br>
2、接口注入<br>
3、Setter：（重要！）<br>
`<bean>`中的scop属性<br>
1、singleton单例<br>
2、proptotype每次创建新的对象<br>
>
自动装配：<br>
byName<br>
byType<br>
如果所有的bean都用同一种，可以使用beans的属性：default-autowire<br>
> 
生命周期：<br>
1、lazy-init<br>
2、init-method destory-method不要和prototype一起使用（scope=”proptoype”调用两次init而没有destroy）//常用（连接池）<br>
Annotation:<br>
1、Autowired(默认按类型bytype；如果想用byName，使用@Qulifier；写在private field（第三种形式注入）不建议破坏封装；如果写在set上，@Qulifer需要写在参数上。)<br>
2、Resource 默认autowire = “byName” 可以指定名称。不足：如果没有源码，就无法使用Annotation，只能使用xml<br>
3、@Component：初始化的名字默认为类名首字母小写；可以指定初始化bean的名字<br>
4、@Scope<br>
5、@PostConstrut=init-method（构造之后调用该方法）；@preDestory=destory-method（销毁之前调用该方法）<br>
> 
AOP：（21、22、23、24、25）<br>
Java实现动态代理：[转载]<br>
1.  public static void main(String args[]) {  
2.          //先将张三相亲这个相亲的实现类实例化，也就是得到XiangQinInterface接口的一个实例对象  <br>
3.          XiangQinInterface zhangSan = new ZhangSanXiangQinInterfaceImpl();  <br>
4.          /** <br>
5.           * 得到ZhangSanXiangQinInterfaceImpl这个类的一个代理类，同时为代理类绑定了一个处理类ReadyInvocationHandler。 <br>
6.           * 听着很绕口，其实就是每次调用ZhangSanXiangQinInterfaceImpl这个子类的xiangQin方法时， <br>
7.           * 不是zhangSan这个ZhangSanXiangQinInterfaceImpl类的实例去调用， <br>
8.           * 而是这个ZhangSanXiangQinInterfaceImpl的代理类ReadyInvocationHandler去调用它自己的invoke方法, <br>
9.           * 这个invoke方法里呢可以调用zhangSan这个实例的xiangQin方法 <br>
10.          */  <br>
11.         /** <br>
12.          * 在java种怎样实现动态代理呢 <br>
13.          * 第一步，我们要有一个接口，还要有一个接口的实现类，而这个实现类呢就是我们要代理的对象， <br>
14.          * 所谓代理呢也就是在调用实现类的方法时，可以在方法执行前后做额外的工作，这个就是代理。 <br>
15.          * 第二步，我们要自己写一个在要代理类的方法执行时，能够做额外工作的类，而这个类必须继承InvocationHandler接口， <br>
16.          * 为什么要继承它呢？因为代理类的实例在调用实现类的方法的时候，不会调真正的实现类的这个方法， <br>
17.          * 而是转而调用这个类的invoke方法（继承时必须实现的方法），在这个方法中你可以调用真正的实现类的这个方法。 <br>
18.          * 第三步，在要用代理类的实例去调用实现类的方法的时候，写出下面两段代码。 <br>
19.          */  <br>
20.         XiangQinInterface proxy = (XiangQinInterface) Proxy.newProxyInstance(  <br>
21.                 zhangSan.getClass().getClassLoader(),  <br>
22.                 zhangSan.getClass().getInterfaces(),  <br>
23.                 new ReadyInvocationHandler(zhangSan));  <br>
24.         proxy.xiangQin();  <br>
25.         /** <br>
26.          * 这里要解释下中部那段长长的代码的意思，以及具体做了哪些工作？ <br>
27.          * 第一，根据zhangSan.getClass().getClassLoader()这个要代理类的类加载器和 <br>
28.          * zhangSan.getClass().getInterfaces()要代理类所实现的所有的接口 <br>
29.          * 作为参数调用Proxy.getProxyClass(ClassLoader loader, Class<?>... interfaces) <br>
30.          * 的方法返回代理类的java.lang.Class对象，也就是得到了java动态生成的代理类$Proxy0的Class对象。 <br>
31.          * 同时，java还让这个动态生成的$Proxy0类实现了要代理类的实现的所有接口，并继承了Proxy接口。 <br>
32.          * 第二，实例化这个动态生成的$Proxy0类的一个实例，实例化代理类的构造函数为Proxy(InvocationHandler h)， <br>
33.          * 也就是说要实例化这个动态生成的$Proxy0类，必须给它一个InvocationHandler参数，也就是我们自己实现的用来在代理类 <br>
34.          * 方法执行前后做额外工作的类ReadyInvocationHandler。 <br>
35.          * 这段代码Proxy.newProxyInstance(zhangSan.getClass().getClassLoader(),zhangSan.getClass().getInterfaces(),new ReadyInvocationHandler(zhangSan)) <br>
36.          * 得到的其实是一个类名叫$Proxy0 extends Proxy implements XiangQinInterface的类。 <br>
37.          * 第三，将这个$Proxy0类强制转型成XiangQinInterface类型，调用xiangQin方法。<br> 
38.          */  <br>
39.     }<br>
><br>
>问题：<br>
>Spring_0850<br>
<br>