>## AngularJS学习笔记
* 编辑：刘彬
* 时间：20xx年x月x日
* 状态：持续更新中。。。
>###<br>
Angular JS通过ng-directives扩展HTML<br>
ng-app指令定义一个AngularJS应用程序，在网页加载完毕时会自动引导应用程序<br>
ng-model指令把元素值(比如输入域的值)绑定到应用程序<br>
1、为应用程序数据提供类型验证(number、email、required)<br>
2、为应用程序数据提供状态(invalid、dirty、touched、error)<br>
3、为HTML元素提供CSS类<br>
4、绑定HTML元素到HTML表单<br>
ng-bind指令把应用程序数据绑定到HTML视图<br>
ng-init指令初始化AngularJS应用程序变量<br>
ng-repeat指令会重复一个HTML元素(对于集合中(数组中)的每一项会克隆一次HTML元素)，可以完美的显示表格。<br>
HTML5允许扩展的（自制的）属性，以 data- 开头。<br>
AngularJS 属性以 ng- 开头，但是您可以使用 data-ng- 来让网页对 HTML5 有效。<br>
AngularJS表达式：<br>
AngularJS表达式写在双大括号内：{{exception}}，可以包含文字、运算符和变量<br>
AngularJS表达式把数据绑定到HTML。<br>
AngularJS将在表达式书写的位置”输出”数据<br>
一个网页可以包含多个运行在不同元素中的 AngularJS 应用程序。<br>
AngularJS 完美支持数据库的 CRUD（增加Create、读取Read、更新Update、删除Delete）应用程序。把实例中的对象想象成数据库中的记录。<br>
AngularJS控制器：<br>
AngularJS控制器控制AngularJS应用程序的数据。<br>
ng-controller指令定义了应用程序控制器<br>
控制器是JavaScript对象，由标准的JavaScript对象的构造函数创建<br>
控制器的$scop是控制器所指向的应用程序HTML元素<br>
控制器也可以把函数作为对象属性<br>
AngularJS过滤器：<br>
AngularJS过滤器可用于数据转换<br>
(currency:格式化数字为货币格式；filter:从数组项中选择一个子集；lowercase:格式化字符串为小写；orderBy:根据某个表达式排列数组；uppercase:格式化字符串为大写)，过滤器通过一个管道字符(|)和一个过滤器添加到表达式中。<br>
输入过滤器可以通过一个管道字符(|)和一个过滤器添加到指令中，该过滤器后跟一个冒号和一个模型名称，例如：<br>
<br>
`<li ng-repeat="x in names | filter:name | orderBy:'country'">`<br>
<br>
    `{{ (x.name | uppercase) + ', ' + x.country }}`
<br>
`</li>`<br>
<br>
AngularJS Http：<br>
`$http`是AngularJS中的一个核心服务，用于读取远程服务器的数据。<br>
`$http.get(url)`是用于读取服务器数据的函数。<br>
`$scope.post(url, data)`向服务器发送POST请求，data为发送参数。<br>
 `$scope`的特性：<br>
 1、scope提供`$watch`方法监视Model的变化。<br>
 2、scope提供`$apply`方法传播Model的变化。<br>
 3、scope可以继承，用来隔离不同的application controller和属性访问权限。<br>
 4、scope为Expressions的计算提供上下文。<br>
应用程序至少应该有一个模块(module)文件和一个控制器(controller)文件<br>
(1)首先创建模块文件“myApp.js”<br>
`var app=angular.module("myApp",[]);`<br>
(2)然后创建控制器文件“myCtrl.js”<br>
`app.controller("myCtrl",function($scope){})`<br>
(3)最后编辑HTML引入模块<br>
`<script scr="myApp.js"></script>`<br>
`<script src="myCtrl.js"></script>`<br>
><br>
(1)`$emit只能向parent controller传递event与data;`<br>
(2)`$broadcast只能向child controller传递event与data;`<br>
(3)`$on用于接收event与data.`<br>
<br>
>### 1、AngularJS `$scope`里的`$apply`方法<br>
作用：scope提供`$scope`方法传播Model的变化。<br>
使用情景：AngularJS 外部的控制器（DOM 事件、外部的回调函数如jQuery UI 空间等）调用了AngularJS函数之后，必须调用`$apply`<br>。在这种情况下，你需要命令AngularJS刷新自已（模型、视图等），`$apply` 就是用来做这件事情的。<br>
注意事项：只要可以，请把要执行的代码和函数传递给`$apply`去执行，而不要自已执行那些函数然后再调用`$apply`。<br>
<br>
>### 2、AngularJS `$watch`方法<br>
作用：`$watch`方法监视Model的变化。<br>
ng-disabled指令直接绑定应用程序数据到HTML的disabled属性<br>
ng-hide/ng-show指令隐藏或显示一个HTML元素(可以使用一个表达式来隐藏和显示html元素)<br>
ng-click指令定义了一个AngularJS单击事件。<br>
在所有的应用程序中，都应该尽量避免使用全局变量和全局函数，全局值(变量或函数)可被其他脚本重写或破坏。<br>
对于HTML应用程序，建议把所有脚本都放在`<body>`<br>元素的最底部，这会提高网页加载速度，因为HTML加载不受制于脚本加载。AngularJS库必须放在AngularJS脚本前面。<br>
HTML属性novalidate用于禁用浏览器的默认验证。<br>
ng-include指令用于在HTML文件中包含HTML文件<br>
<br>
>### 3、AngularJS `$scope`里的`$digest`方法<br>
作用：该方法会触发当前scope以及child scope中的所有watchers，因为watcher的listener可能会改变model，所以`$digest`<br>方法会一直触发watchers直到不再有listener被触发。当然这也有可能会导致死循环，不过angular也帮我们设置了上限 10 ！否则会抛出“ Maximum iteration limit exceeded. ”。 通常，我们不在controller或者directive中直接调用`$digest`方法，而是调`$apply`方法，让`$apply`方法去调用`$digest`方法。 <br>
<br>